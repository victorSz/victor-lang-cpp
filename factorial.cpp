#include <cstdio>
#include <cstdlib>
#include <functional>

#include "lang.h"
#include "stdlang.h"

std::function<int(int)> factorial = [](int x) {
    return (lang::less < 2 < x) ? 1 : 
        1 > lang::subtract < x > factorial > lang::multiply < x;
};

int main(int argc, char *argv[]) {
    std::function<int(char*)> atoi = std::atoi;
    std::printf("%d\n", argv[1] > atoi > factorial);
}
