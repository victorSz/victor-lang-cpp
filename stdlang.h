#ifndef stdlang_h_INCLUDED
#define stdlang_h_INCLUDED

#include <functional>

namespace lang {
extern std::function<std::function<int(int)>(int)> add;
extern std::function<std::function<int(int)>(int)> subtract;
extern std::function<std::function<int(int)>(int)> multiply;
extern std::function<std::function<int(int)>(int)> divide;

extern std::function<std::function<int(int)>(int)> equal;
extern std::function<std::function<int(int)>(int)> less;
}

#endif // stdlang_h_INCLUDED
