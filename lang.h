#ifndef lang_h_INCLUDED
#define lang_h_INCLUDED

#include <functional>

template <typename InputType, typename ResultType>
ResultType operator>(InputType input, std::function<ResultType(InputType)> function) {
    return function(input);
}

template <typename InputType, typename ResultType>
ResultType operator<(std::function<ResultType(InputType)> function, InputType input) {
    return function(input);
}

#endif // lang_h_INCLUDED
