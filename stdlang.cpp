#include "stdlang.h"

template <typename Op>
static std::function<int(int)> bin_op(int y) {
    return [y](int x) -> int { return Op{}(x, y); };
}

std::function<std::function<int(int)>(int)> lang::add = bin_op<std::plus<int>>;
std::function<std::function<int(int)>(int)> lang::subtract = bin_op<std::minus<int>>;
std::function<std::function<int(int)>(int)> lang::multiply = bin_op<std::multiplies<int>>;
std::function<std::function<int(int)>(int)> lang::divide = bin_op<std::divides<int>>;

std::function<std::function<int(int)>(int)> lang::equal = bin_op<std::equal_to<int>>;
std::function<std::function<int(int)>(int)> lang::less = bin_op<std::less<int>>;
